import { Component, OnInit } from '@angular/core';
import { InputModel } from 'src/app/models/input';
import { Protractor } from 'src/app/models/protractor';
import { Result } from 'src/app/models/result';
import { Log } from 'src/app/models/Log';

@Component({
  selector: 'app-protractor',
  templateUrl: './protractor.component.html',
  styleUrls: ['./protractor.component.css']
})
export class ProtractorComponent implements OnInit {

  nameAmountSegment: string = "amountSegment";
  nameTotalTime: string = "totalTime";
  nameLastSegmentSize: string = "lastSegmentSize";
  nameEndDate: string = "endDate";

  /**
   * Títulos dos campos
   */
  titleAmountSegment: string = "Quantidade de Segmentos Enviados";
  titleTotalTime: string = "Tempo Total";
  titleLastSegmentSize: string = "Tamanho do Último Segmento";
  titleEndDate: string = "Data e Hora Final";

  input: InputModel;
  protractor: Protractor;
  result: Result;
  log: Log;
  
  constructor() { }

  ngOnInit() {
    this.cleanResult();
  }

  public startTransmit(input: InputModel): void {
    this.input = input;

    this.protractor.seq = this.getSeq(this.input.mss);
    this.protractor.ack = this.getAck(this.input.mss);
    this.protractor.transferTime = this.getTransferTime(this.input.mss, 
                                                        this.input.transferRate);
    this.protractor.transferTimeSegment = this.getTransferTimeSegment(this.protractor.transferTime, 
                                                                      this.input.delay);
    this.result.lastSegmentSize = this.getLastSegmentSize(this.input.fileSize, 
                                                          this.input.mss);
    this.protractor.lastSegmentTime = this.getLastSegmentTime(this.result.lastSegmentSize, 
                                                              this.input.transferRate, 
                                                              this.input.delay);
    this.result.amountSegment = this.getAmountSegment(this.input.fileSize, 
                                                      this.input.mss);
    this.result.totalTime = this.getTotalTime(this.protractor.transferTimeSegment, 
                                              this.result.amountSegment, 
                                              this.protractor.lastSegmentTime, 
                                              this.result.lastSegmentSize, 
                                              this.input.mss);
    this.result.finalDate = this.getFinalDate(this.input.startDate, 
                                              this.result.totalTime);
    this.log.start = this.sendTotransmit(0, this.protractor.seq, this.protractor.ack);
  }

  public getSeq(mss: number): number {
    return (mss - 1);
  }

  public getAck(mss: number): number {
    return mss;
  }

  public getTransferTime(mss: number, transferRate: number): number {
    return this.secondToMicrosecondsConverter(mss / this.bytesConverter(transferRate));
  }

  public getTransferTimeSegment(transferTime: number, delay: number): number {
    return (transferTime + delay) * 2;
  }

  public getLastSegmentSize(fileSize: number, mss: number): number {
    let lastSegmentSize = this.bytesConverter(fileSize) % mss;
    return lastSegmentSize === 0 ? mss : lastSegmentSize;
  }

  public getLastSegmentTime(lastSegmentSize: number, transferRate: number, delay: number): number {
    return (this.secondToMicrosecondsConverter(lastSegmentSize / this.bytesConverter(transferRate)) + delay) * 2;
  }

  public getAmountSegment(fileSize: number, mss: number): number {
    return Math.ceil(this.bytesConverter(fileSize) / mss);
  }

  public getTotalTime(transferTimeSegment: number, amountSegment: number, lastSegmentTime: number, 
    lastSegmentSize: number, mss: number): number {
    if(lastSegmentSize != mss) {
      amountSegment--;
      lastSegmentTime = 0;
    }
    return (transferTimeSegment * amountSegment) + lastSegmentTime;
  }

  public getFinalDate(startDate: Date, totalTime: number): string {
    totalTime = this.microsecondsToMillisecondsConverter(totalTime);
    let date = new Date(new Date(startDate).getTime() + totalTime);
    let strDate = date.toLocaleDateString();
    let hour = this.checkZero(date.getHours());
    let minutes = this.checkZero(date.getMinutes());
    let seconds = this.checkZero(date.getSeconds());
    let millis = this.checkZero(date.getMilliseconds());
    return strDate + " " + hour + ":" + minutes + ":" + seconds + ":" + millis;
  }

  public bytesConverter(megaBytes: number): number {
    return (megaBytes * 1024) * 1024;
  }

  public secondToMicrosecondsConverter(seconds: number): number {
    return seconds * 1000000;
  }

  public microsecondsToMillisecondsConverter(microseconds: number): number {
    return microseconds / 1000;
  }

  public checkZero(data: number): string{
    let info = data + "";
    return info.length == 1 ? "0" + info : info;
  }

  public moreLog() {
    this.log.start = this.sendTotransmit(this.log.start, this.log.seq, this.log.ack);
  }

  public sendTotransmit(start: number, seq: number, ack: number): number {
      return this.transmit(start, 
                           this.result.amountSegment, 
                           seq,
                           ack,
                           this.protractor.transferTimeSegment, 
                           this.protractor.lastSegmentTime, 
                           this.result.lastSegmentSize);
  }

  public transmit(start: number, amountSegment: number, seq: number, ack: number,
    transferTimeSegment: number, lastSegmentTime: number, lastSegmentSize: number): number {
    this.log.seq = seq;
    this.log.ack = ack;
    if (start != 0)
      this.log.lastLog = [];
    let max = 1000;
    var limit = ((amountSegment > max ? max : amountSegment) - 1) + start;
    let timeTransfer = transferTimeSegment / 2;

    do {
      this.generateLog(this.log.logs, start + 1, this.log.seq, timeTransfer, 
        this.log.ack, timeTransfer);
      this.log.seq += this.input.mss;
      this.log.ack += this.input.mss;
      start++;
    } while(start <= limit);
    this.log.showMore = start < (amountSegment - max);
    this.generateLastLog(amountSegment, lastSegmentTime, lastSegmentSize, this.log.showMore);
    return start;
  }

  public generateLastLog(amountSegment: number, lastSegmentTime: number, lastSegmentSize: number, 
    showMore: boolean): void {
    lastSegmentTime = lastSegmentTime / 2;
    if (showMore) {
      for (var i=0; i < 3; i++)
        this.log.lastLog.push(".");
    }
    this.generateLog(this.log.lastLog, amountSegment, (lastSegmentSize - 1), lastSegmentTime, 
      lastSegmentSize, lastSegmentTime);
  }

  public generateLog(logs: string[], nuSegmento: number, seq: number, timeTransfer: number, ack: number, 
      timeReturn: number): void {
    logs.push("--------------------------------------------------------------");
    logs.push("Segmento número [ " + nuSegmento + " ]");
    this.generateTransferLog(logs, seq, timeTransfer);
    this.generateReceiveLog(logs, ack, timeReturn);
  }

  public generateTransferLog(logs: string[], seq: number, timeTransfer: number): void {
    logs.push("--| Dados transmitidos:");
    logs.push(" SEQ: [ " + seq + " ] Bytes");
    logs.push(" tempo de fransferência: [ " + timeTransfer + " ] µs");
  }

  public generateReceiveLog(logs: string[], ack: number, timeReturn: number): void {
    logs.push("--| Dados retornados:");
    logs.push(" ACK: [ " + ack + " ] Bytes");
    logs.push(" tempo de retorno: [ " + timeReturn + " ] µs");
  }

  public cleanResult(): void {
    this.log = new Log(0, ["Aguardando transmissão_"], [], false, null, null);
    this.protractor = new Protractor(null, null, null, null, null);
    this.result = new Result(null, null, "", null, false);
  }

  public disableBtnClean(): boolean {
    return this.log
        && this.log.start == 0;
  } 

}
