import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputModel } from 'src/app/models/input';
import { Log } from 'src/app/models/Log';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.css']
})
export class ConsoleComponent implements OnInit {

  @Input() input: InputModel;
  @Input() log: Log;
  @Output() moreLog = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public more() {
    this.moreLog.emit(JSON.parse(JSON.stringify("more")));
  }

}
