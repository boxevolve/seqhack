import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { InputModel } from 'src/app/models/input';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  /**
   * Nomes dos formControlName dos campos
   */
  nameFileSize: string = "fileSize";
  nameMss: string = "mss";
  nameTransferRate: string = "transferRate";
  nameStartDate: string = "startDate"
  nameDelay: string = "delay"

  /**
   * Títulos dos campos
   */
  titleFileSize: string = "Tamanho do arquivo";
  titleMss: string = "MSS(Maximum Segment Size)";
  titleTransferRate: string = "Taxa de Transferência";
  titleStartDate: string = "Data e Hora Inicial";
  titleDelay: string = "Delay"
  
  form: FormGroup;

  input: InputModel;
  @Output() transmitData = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.createFormGroup();
  }

  public createFormGroup(): void {
    //Criando formulário reativo
    this.form = this.formBuilder.group({
      fileSize: [null, [ Validators.required ]],
      mss: [null, [ Validators.required ]],
      transferRate: [null, [ Validators.required ]],
      startDate: [null, [ Validators.required ]],
      delay: [null, [ Validators.required ]]
    });
  }

  public isValid(field: string): boolean {
    let formControl = this.form.get(field);
    return formControl.invalid && (formControl.touched || formControl.dirty);
  }

  public requiredMessage(field: string): string {
    return "Por favor, informe o " + field + ".";
  }

  public transmit(): void {
    if(this.form.invalid) {
      return;
    }
    this.input = JSON.parse(JSON.stringify(this.form.value));
    this.transmitData.emit(this.input);
  }

}
