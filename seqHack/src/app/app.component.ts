import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { InputModel } from './models/input';
import { ProtractorComponent } from './components/protractor/protractor.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild("protractorComponent") protractorComponent: ProtractorComponent;

  constructor() { }

  ngOnInit() {

  }

  public reciverData(input: InputModel) {
    this.protractorComponent.startTransmit(input);
  }

}
