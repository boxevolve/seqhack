export class InputModel {
    constructor(
        public fileSize: number, 
        public mss: number,
        public transferRate: number,
        public startDate: Date,
        public delay: number){}
}
