export class Protractor {
    constructor(
        public seq: number,
        public ack: number,
        public transferTime: number,
        public transferTimeSegment: number,
        public lastSegmentTime: number){}
}
