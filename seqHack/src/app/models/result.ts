export class Result {
    constructor(
        public amountSegment: number,
        public totalTime: number,
        public finalDate: string,
        public lastSegmentSize: number,
        public isClean: boolean){}
}
