export class Log {
    constructor(
        public start: number,
        public logs: string[],
        public lastLog: string[],
        public showMore: boolean,
        public seq: number,
        public ack: number){}
}
